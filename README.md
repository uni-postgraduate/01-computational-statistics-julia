[![][binder-img]][binder-url]

## Computational Statistics in Julia

[binder-img]: https://mybinder.org/badge_logo.svg
[binder-url]: https://mybinder.org/v2/gl/ErickChacon%2Fcourse-computational-statistics-julia/HEAD?labpath=notebooks
